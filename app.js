const express = require('express');
const stripe = require('stripe')('sk_test_Sy31l2o6FWKrXjHB9Ixj0WKI00m2h5Grqn');
const bodyParser = require('body-parser');
const exphbs = require('express-handlebars');

const app = express();

//Handlebars Middleware
app.engine('handlebars',exphbs({defaultlayout:'main'}));
app.set('view engine', 'handlebars');

//Body Parser Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

//set static folder
app.use(express.static(`${__dirname}/public`));

//index route
app.get('/', (req, res) => {
    res.render('index');
}) 



//charge route
app.post('/charge', (req, res,)=> {
    const amount = 2500;

    stripe.customers.create({
        email: req.body.stripeEmail,
        source : req.body.stripeToken
    })
    .then(customer => stripe.charges.create({
       amount,
       description: 'Web Development Ebook',
       currency: 'usd',
       customer: customer.id 
    }))
    .then(charge => res.render('success'));
}); 

const port = process.env.PORT || 5000;

app.listen(port, () => {
    console.log (`server started on port ${port}`);
});   